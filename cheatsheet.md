#Creating and Resizing Canvas
 set height and width with JS

1 select canvas::
  const canvas = document.querySelector('#canvas');;

2 canvas width and height
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

passing functions and methods to c
const c = canvas.getContext('2d');

rectangle
  c.fillRect(x, y, width, height);


#Drawing on canvas
Objects:
  rectangles
  lines
  arcs
  bezier-curves
  images
  text

##draw a line
c.beginPath();
c.moveTo(x, y); //Invisible till stroke method is called
c.lineTo(x, y);
c.strokeStyle =  "#fa34a3";//like any color Value
c.fillStyle = "red"; //fills shape with color:
c.stroke(); //give color to path

##draw a arc (circle)
c.arc(x: int, y: Int, r: radius, startAngle: int, endAngle: int);
c.beginPath();
c.arc(300, 300, 30, 0, Math.PI times 2, false);
c.stroke();

##multiple circle with for loop
for(let i = 0; i < 3; i++) {
  let x = Math.random() * window.innerWidth;
  let y = Math.random() * window.innerHeight;
  c.beginPath();
  c.arc(300, 300, 30, 0, Math.PI times 2, false);
  c.strokeStyle = 'blue';
  c.stroke();
}


#Animations
//Create Object
function Circle(x, y, dx, dy) {
  this.x = x;
  this.y = y;
  this.dx = dx;
  this.dy = dy;

  this.draw = function() {
    c.beginPath();
    c.arc(x, y, radius, 0, Math.PI times 2, false);
    c.strokeStyle = 'blue';
    c.stroke();
  }
  //move circle
  this.update = function() {
    //condition to bounce back once it hits right side of screen
    //Increment on each draw -> move
    if (this.x + radius > innerWidth || this.x - radius < 0) {
        this.dx = -this.dx
        this.x += this.dx;
    }

    if(this.y + radius > innerHeight || this.y - radius < 0) {
      this.y += this.dy;
    }

    this.x  += this.dx;
    this.y += this.dy

    this.draw();
  }
}

var circle = new Circle(200, 200, 3,3,30);


let x = Math.random() * innerWidth; //random x span location
let y = Math.random() * innerHeight; //random y span location
//velocity --> movespeed
let dx = (Math.random() - 0.5) * 2; // get positve or negative value between 0.5 and -0.5 increment speed with * 2
let dy = Math.random() - 0.5;
let radius = 30;

//Loop
function animate() {
  requestAnimationFrame(animate);
  //clear canvas for next iteration
  c.clearRect(0, 0, innerWidth, innerHeight);

}

animate();


animation refresh page and change values --> illusion of animation
