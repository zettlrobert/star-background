function init() {
  //On Document Ready, execute Script
  console.log("script running");
  const d = document;

  //Select Canvas and context
  const canvas = d.querySelector('#canvas');
  const ctx = canvas.getContext('2d');

  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  //Mouse Position
  let mouse = {
    x: undefined,
    y: undefined
  }
  //Mousmove Event
  window.addEventListener('mousemove', function(event) {
    mouse.x = event.x;
    mouse.y = event.y;
    // console.log(mouse);
  })

  // Colors for Stars
  let innerStarColor = [
    // "rgba(238, 119, 82, 0.5)",
    // "rgba(231, 60, 126, 0.5)",
    // "rgba(35, 166, 213, 0.5)",
    // "rgba(35, 213, 171, 0.5)",

    'rgba(255,255,255, 0.5)'
  ]

  let outerStarColor = [
    // "rgba(238, 119, 82, 0.2)",
    // "rgba(231, 60, 126, 0.2)",
    // "rgba(35, 166, 213, 0.2)",
    // "rgba(35, 213, 171, 0.2)",

    'rgba(255,255,255,0.2)'
  ]


  //Create Star Object
  function Star(x, y, radius) {
    //let x = 100;
    this.x = x;
    //let y = 100;
    this.y = y;

    //Colors
    let innerColor = innerStarColor[Math.floor(Math.random() * innerStarColor.length)];
    let outerColor = outerStarColor[Math.floor(Math.random() * outerStarColor.length)];
    //Radi Glow
    let innerRadius = Math.floor(Math.random() + 1); //Inner Circle Star
    let outerRadius = innerRadius * 3; //Outer Circle Star
    //let radius = 10; //Star Radius
    this.radius = radius;

    this.draw = function() {
      ctx.beginPath();
      //Gradient for Star
      //Gradient
      // x.createRadialGradient(x0 startCircle, y0 startCircle, r0 radiusCircle, x1 endCircle, y1 endCircle, radEndCircle)
      let grd = ctx.createRadialGradient(x, y, innerRadius, x, y, outerRadius);
      //inner Color
      grd.addColorStop(0, innerColor);
      //outerColor
      grd.addColorStop(0.3, outerColor);
      //Faded
      grd.addColorStop(1, "rgba(255, 255, 255, 0.1)");

      // Create Circle
      ctx.arc(x, y, radius, 0, Math.PI * 2);
      // Fill with Gradient
      ctx.fillStyle = grd;
      ctx.fill();
    }

    // this.update = function() {
    //   ctx.save();
    //   let square = {
    //     x: '730',
    //     y: '80',
    //     w: '200',
    //     h: '200'
    //   }
    //   // ctx.clearRect(square.x, square.y,square.w,square.h);
    //   ctx.arc(stars[0].x, stars[0].y, 5, 0, Math.PI * 3);
    //   ctx.fillStyle = 'red';
    //   ctx.fill();
    // }
  }


  let stars = [];
  let backgroundCount = window.innerWidth * window.innerHeight / 1500;

  for (let i = 0; i < backgroundCount; i++) {
    let x = Math.random() * window.innerWidth;
    let y = Math.random() * window.innerHeight;
    let radius = (Math.random() + Math.random() * 3);
    stars.push(new Star(x, y, radius));
    stars[i].draw();
  }
  console.log(stars.length);
  console.table(stars[0]);

  // console.table(stars);
  function change() {
    function change(stars)  {
      stars.forEach((star) => {
        x += 1;
      })
    }
  change();

  //////////////////////////7
  //Second Canas Element, for Telescope
  const secondCanvas = d.querySelector('#secondCanvas');
  console.log(secondCanvas);
  const secondCtx = secondCanvas.getContext('2d');

  secondCanvas.width = window.innerWidth;
  secondCanvas.height = window.innerHeight;

  function telescope() {
    //Clear for new Shape after animation
    secondCtx.clearRect(0, 0, innerWidth, innerHeight);

    let secondRadius = window.innerWidth / 50;

    secondCtx.beginPath();
    secondCtx.arc(mouse.x, mouse.y, secondRadius, Math.PI * 2, false);
    secondCtx.fillStyle = "rgba(0, 219, 219, 0.3)";
    secondCtx.strokeStyle = 'silver';
    secondCtx.lineWidth = 1.5;
    secondCtx.stroke();
    secondCtx.fill();
    //Prevent Telescope Cutoff
    // if(mouse.x < 50 || mouse.x > innerWidth - 50)

    telescopeMove();

    //Loop with itself
    requestAnimationFrame(telescope);
  }
  telescope();



  //if requestAniamtion Frame telescope check every star
  function telescopeMove() {
    //Check Region
    let telescopePosition = {
      posX: "",
      posY: ""
    }

    window.addEventListener('mousemove', function(event) {
      telescopePosition.posX = event.x;
      telescopePosition.posY = event.y;


    })

  }





}
window.addEventListener('load', init);


/*
1. Create Object
2. Add Eventlistener for mouseover and mouseleave
3. update if function mouseover is called
4. reset if mouseleave is called


 */
