const gulp = require('gulp');
const sync = require('browser-sync');


//Static Server
gulp.task('sync', function() {
  sync.init({
    server: {
      baseDir: "src/"
    }
  });

  //Select What to Watch for Sync
  gulp.watch(['src/']).on('change', sync.reload);

});
